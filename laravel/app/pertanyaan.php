<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    protected $fillable = ['judul','isi','created_at','updated_at'];
}
