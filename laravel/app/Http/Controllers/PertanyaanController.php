<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pertanyaan;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = pertanyaan::all();
        return view('fitur_pertanyaan.menampilkan_pertanyaan',compact(['datas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           return view('fitur_pertanyaan.membuat_pertanyaan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Pertanyaan;
        $data->judul = $request->judul;
        $data->isi = $request->isi;
        $data->save();
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = pertanyaan::where('id',$id)->first();
        return view ('fitur_pertanyaan.detail_pertanyaan',compact(['data']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = pertanyaan::where('id',$id)->first();
        return view ('fitur_pertanyaan.edit_pertanyaan',compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        pertanyaan::where('id',$id)->update([
            'judul'=>$request->judul,
            'isi'=>$request->isi
            ]);
        return redirect()->action(
            [PertanyaanController::class, 'show'], [$id]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=pertanyaan::find($id);
        $data->delete();
        return redirect('pertanyaan');
    }
}
