<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Halaman Utama
Route::view('/','dashboard.welcome');
//Data Table
Route::view('/data-tables','dataTable.table');
//Fitur Pertanyaan
Route::resource('/pertanyaan','PertanyaanController');