@extends('layout.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Pertanyaan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Pertanyaan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <a href="/pertanyaan/create" class="btn btn-primary"> Buat Pertanyaan baru <i class="fas fa-plus-square"></i></a>
        <br>
        <br>
        @forelse ($datas as $data)
        <div class="card card-default">
            <!-- /.card-header -->
            <div class="card-body text-center">
                <a href="/pertanyaan/{{$data->id}}" class="float-left"><h3>{{$data->judul}}</h3></a>
                <p class="d-flex justify-content-end">Ditanyakan pada {{$data->created_at}}</p>
            </div>
            <!-- /.card-body -->
        </div>
        @empty
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Tidak ada Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body text-center">
                <h3>Buat Pertanyaan baru </h3>
               <a href="/pertanyaan/create" class="btn btn-primary"> <i class="fas fa-plus-square"></i></a>
                
            </div>
            <!-- /.card-body -->
        </div>

        @endforelse
    </div>
</section>

@endsection
