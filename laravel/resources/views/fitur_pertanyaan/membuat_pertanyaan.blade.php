@extends('layout.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Membuat Pertanyaan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/pertanyaan">Pertanyaan</a></li>
                    <li class="breadcrumb-item active">Membuat Pertanyaan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Ajukan pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <form action="/pertanyaan" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>Judul Pertanyaan</label>
                        <input type="text" class="form-control" placeholder="Masukan Judul Pertanyaan" name="judul">
                    </div>
                    <div class="form-group">
                        <label>Isi</label>
                        <textarea name ="isi" placeholder="Place some text here"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>

                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">Submit <i
                                class="fas fa-paper-plane"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card -->
        <!-- /.card-body -->
    </div>
</section>

@endsection

