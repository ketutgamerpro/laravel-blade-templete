@extends('layout.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Membuat Pertanyaan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/pertanyaan">Pertanyaan</a></li>
                    <li class="breadcrumb-item active">Detail Pertanyaan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">{{$data->judul}}</h3>
            </div>
            <!-- /.card-header -->
                <div class="card-body">
                    {{$data->isi}}

                    <!-- /.card-body -->

                    <div class="card-footer">
                        <form action="/pertanyaan/{{$data->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <a href="/pertanyaan/{{$data->id}}/edit" class="btn btn-primary float-right">Edit
                                    pertanyaan
                                    <i class="fas fa-pencil-alt"></i></a>
                                <p>&emsp;</p>
                                 <button type="submit" class="btn btn-danger float-right">Hapus Pertanyaan 
                                     <i class="fas fa-trash-alt"></i></button>
                            </div>

                        </form>

                    </div>
                </div>
        </div>
        <!-- /.card -->
        <!-- /.card-body -->
    </div>
</section>

@endsection
